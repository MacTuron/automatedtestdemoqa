
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.executors.HomePage;

import static org.junit.Assert.assertTrue;

public class HomePageTabsTests {

    private WebDriver driver;

    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\drivers\\chromedriver.exe");

        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://demoqa.com/");
    }

    @Test
    public void testTab1() {
        HomePage homePage = new HomePage(driver);
        homePage.clickTab1();
        assertTrue("Tab 1 title was not displayed", homePage.isContentTitle1Displayed());
    }

    @Test
    public void testTab2() {
        HomePage homePage = new HomePage(driver);
        homePage.clickTab2();
        assertTrue("Tab 2 title was not displayed", homePage.isContentTitle2Displayed());
    }

    @Test
    public void testRegistrationButton() {
        HomePage homePage = new HomePage(driver);
        homePage.clickReg();
    }
}
