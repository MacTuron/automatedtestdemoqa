import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.executors.RegistrationPage;

import static org.junit.Assert.assertTrue;

public class RegistrationPageTest {

    private WebDriver driver;

    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\drivers\\chromedriver.exe");

        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://demoqa.com/registration");
    }

    @Test
    public void fillForm() {
        RegistrationPage registrationPage = new RegistrationPage(driver);
        registrationPage.enterFirstName("Maciej3");
        registrationPage.enterLastName("Turon");
        registrationPage.selectMaritalStatus();
        registrationPage.selectHobby();
        registrationPage.selectCountry("Poland");
        registrationPage.selectMonth("10");
        registrationPage.selectDay("22");
        registrationPage.selectYear("1989");
        registrationPage.enterPhoneNumber("48888999777");
        registrationPage.enterUserName("mturon3");
        registrationPage.enterEmail("maciejturon3@gmail.com");
        registrationPage.addProfilePicture();
        registrationPage.enterDescription("Example description");
        registrationPage.enterPassword("Password1234");
        registrationPage.enterConfirmPassword("Password1234");
        assertTrue("Very weak text was not displayed", registrationPage.isContentPasswordMeterDisplayed());
        registrationPage.clickSubmitButton();
        assertTrue("Confirmation message was not displayed", registrationPage.isConfirmationMessageDisplayed());
    }
}
