package pages.locators;

import com.sun.xml.internal.bind.v2.model.core.ID;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class HomePageLocators {

    @FindBy(how = How.ID, using = "ui-id-1")
    public WebElement tab1Button;

    @FindBy(how = How.ID, using = "ui-id-2")
    public WebElement tab2Button;

    @FindBy(how = How.ID, using = "ui-id-3")
    public WebElement tab3Button;

    @FindBy(how = How.ID, using = "ui-id-4")
    public WebElement tab4Button;

    @FindBy(how = How.ID, using = "ui-id-5")
    public WebElement tab5Button;

    @FindBy(how = How.CSS, using = "#tabs-1>b")
    public WebElement contentTitle1;

    @FindBy(how = How.CSS, using = "#tabs-2>b")
    public WebElement contentTitle2;

    @FindBy(how = How.CSS, using = "#tabs-3>b")
    public WebElement contentTitle3;

    @FindBy(how = How.CSS, using = "#tabs-4>b")
    public WebElement contentTitle4;

    @FindBy(how = How.CSS, using = "#tabs-5>b")
    public WebElement contentTitle5;

    @FindBy(how = How.CSS, using = "#menu-item-374>a")
    public WebElement registrationButton;

}
