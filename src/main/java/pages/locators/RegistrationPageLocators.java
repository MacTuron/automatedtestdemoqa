package pages.locators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class RegistrationPageLocators {

    @FindBy(how = How.ID, using = "name_3_firstname")
    public WebElement firstNameInput;

    @FindBy(how = How.ID, using = "name_3_lastname")
    public WebElement lastNameInput;

    @FindBy(how = How.CSS, using = "input[value='married']")
    public WebElement maritalStatusSelect;

    @FindBy(how = How.CSS, using = "input[value='dance']")
    public WebElement hobbySelect;

    @FindBy(how = How.ID, using = "dropdown_7")
    public WebElement countrySelect;

    @FindBy(how = How.ID, using = "mm_date_8")
    public WebElement monthSelect;

    @FindBy(how = How.ID, using = "dd_date_8")
    public WebElement daySelect;

    @FindBy(how = How.ID, using = "yy_date_8")
    public WebElement yearSelect;

    @FindBy(how = How.ID, using = "phone_9")
    public WebElement phoneNumberInput;

    @FindBy(how = How.ID, using = "username")
    public WebElement userNameInput;

    @FindBy(how = How.ID, using = "email_1")
    public WebElement emailInput;

    @FindBy(how = How.ID, using = "profile_pic_10")
    public WebElement profilePictureButton;

    @FindBy(how = How.ID, using = "description")
    public WebElement aboutYourselfInput;

    @FindBy(how = How.ID, using = "password_2")
    public WebElement passwordInput;

    @FindBy(how = How.ID, using = "confirm_password_password_2")
    public WebElement confirmPasswordInput;

    @FindBy(how = How.XPATH, using = "//*[@class='fieldset']//div[.='Very weak']")
    public WebElement veryWeakMessage;

    @FindBy(how = How.XPATH, using = "//*[contains(@type,'submit')]")
    public WebElement submitButton;

    @FindBy(how = How.XPATH, using = "//*[@class='piereg_message']")
    public WebElement confirmationMessage;

    // Example comment

}
