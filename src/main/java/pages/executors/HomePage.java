package pages.executors;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import pages.locators.HomePageLocators;

public class HomePage {

    private HomePageLocators locators;

    public HomePage(WebDriver driver){
        locators = new HomePageLocators();
        PageFactory.initElements(driver, locators);
    }

    public void clickTab1() {
        locators.tab1Button.click();
    }

    public void clickTab2() {
        locators.tab2Button.click();
    }

    public void clickTab3() {
        locators.tab3Button.click();
    }

    public void clickTab4() {
        locators.tab4Button.click();
    }

    public void clickTab5() {
        locators.tab5Button.click();
    }

    public boolean isContentTitle1Displayed(){
        return locators.contentTitle1.isDisplayed();
    }

    public boolean isContentTitle2Displayed(){
        return locators.contentTitle2.isDisplayed();
    }

    public boolean isContentTitle3Displayed(){
        return locators.contentTitle3.isDisplayed();
    }

    public boolean isContentTitle4Displayed(){
        return locators.contentTitle4.isDisplayed();
    }

    public boolean isContentTitle5Displayed(){
        return locators.contentTitle5.isDisplayed();
    }

    public void clickReg() {
        locators.registrationButton.click();
    }

}
