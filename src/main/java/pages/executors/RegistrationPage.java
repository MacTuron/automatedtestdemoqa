package pages.executors;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import pages.locators.RegistrationPageLocators;

public class RegistrationPage {

    private RegistrationPageLocators locators;
    private String pathToPicture = "C:\\Users\\mturon\\test.png";

    public RegistrationPage(WebDriver driver) {
        locators = new RegistrationPageLocators();
        PageFactory.initElements(driver, locators);
    }

    public RegistrationPage enterFirstName(String name) {
        locators.firstNameInput.sendKeys(name);
        return this;
    }

    public RegistrationPage enterLastName(String name) {
        locators.lastNameInput.sendKeys(name);
        return this;
    }

    public RegistrationPage selectMaritalStatus() {
        locators.maritalStatusSelect.click();
        return this;
    }

    public RegistrationPage selectHobby() {
        locators.hobbySelect.click();
        return this;
    }

    public RegistrationPage selectCountry(String country) {
        Select select = new Select(locators.countrySelect);
        select.selectByVisibleText(country);
        return this;
    }

    public RegistrationPage selectMonth(String month) {
        Select select = new Select(locators.monthSelect);
        select.selectByVisibleText(month);
        return this;
    }

    public RegistrationPage selectDay(String day) {
        Select select = new Select(locators.daySelect);
        select.selectByVisibleText(day);
        return this;
    }

    public RegistrationPage selectYear(String year) {
        Select select = new Select(locators.yearSelect);
        select.selectByVisibleText(year);
        return this;
    }

    public RegistrationPage enterPhoneNumber(String number) {
        locators.phoneNumberInput.sendKeys(number);
        return this;
    }

    public RegistrationPage enterUserName(String userName) {
        locators.userNameInput.sendKeys(userName);
        return this;
    }

    public RegistrationPage enterEmail(String email) {
        locators.emailInput.sendKeys(email);
        return this;
    }

    public void addProfilePicture() {
        locators.profilePictureButton.sendKeys(getPicturePath());
    }

    public String getPicturePath() {
        return pathToPicture;
    }

    public RegistrationPage enterDescription(String description) {
        locators.aboutYourselfInput.sendKeys(description);
        return this;
    }

    public RegistrationPage enterPassword(String password) {
        locators.passwordInput.sendKeys(password);
        return this;
    }

    public RegistrationPage enterConfirmPassword(String password) {
        locators.confirmPasswordInput.sendKeys(password);
        return this;
    }

    public boolean isContentPasswordMeterDisplayed() {
        return locators.veryWeakMessage.isDisplayed();
    }

    public void clickSubmitButton() {
        locators.submitButton.click();
    }

    public boolean isConfirmationMessageDisplayed() {
        return locators.confirmationMessage.isDisplayed();
    }

}
